/*
 * Example program to test metadata extraction.
 */

#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include "audecode.h"
#include "test_lib.h"

int test_read_artist(char *input_file) {
  audecode_t p;
  audecode_metadata_iterator_t mdi;
  char *key, *value;
  struct fdctx ctx;
  int r, count = 0;

  ctx.fd = open(path_of(input_file), O_RDONLY);
  r = audecode_new(&p, 4096, &my_read_cb, (void *)&ctx);
  if (r < 0) {
    die("audecode_new", r);
  }

  mdi = audecode_get_metadata(p);
  if (mdi == NULL) {
    die_msg("audecode_get_metadata() returned NULL");
  }

  while (0 == audecode_metadata_next(mdi, &key, &value)) {
    fprintf(stderr, "md: %s = %s\n", key, value);
    count++;
    if (strcasecmp(key, "artist") != 0) {
      die_msg("bad audecode metadata key");
    }
    if (strcmp(value, "King Crimson") != 0) {
      die_msg("bad audecode metadata value");
    }
  }
  if (count == 0) {
    die_msg("did not read any metadata");
  }
  if (count != 1) {
    die_msg("read too much metadata");
  }

  audecode_close(p);

  return 0;
}

int test_read_any_meta(char *input_file) {
  audecode_t p;
  audecode_metadata_iterator_t mdi;
  char *key, *value;
  struct fdctx ctx;
  int r, count = 0;

  ctx.fd = open(path_of(input_file), O_RDONLY);
  r = audecode_new(&p, 4096, &my_read_cb, (void *)&ctx);
  if (r < 0) {
    die("audecode_new", r);
  }

  mdi = audecode_get_metadata(p);
  if (mdi == NULL) {
    fprintf(stderr, "ERROR: audecode_get_metadata(%s) returned NULL\n", input_file);
    return 1;
  }

  while (0 == audecode_metadata_next(mdi, &key, &value)) {
    fprintf(stderr, "md: %s = %s\n", key, value);
    count++;
  }
  if (count == 0) {
    fprintf(stdout, "ERROR: found no metadata in %s\n", input_file);
    audecode_dump_metadata(p);
    return 1;
  }

  audecode_close(p);

  return 0;
}

int main(int argc, char **argv) {
  int errors = 0;

  audecode_init();

  fprintf(stdout, "test_read_artist\n");
  errors += test_read_artist(TEST_INPUT);
  fprintf(stdout, "test_read_any_meta\n");
  errors += test_read_any_meta("testdata/meta1.mp3");
  errors += test_read_any_meta("testdata/meta1.flac");
  errors += test_read_any_meta("testdata/meta1.ogg");

  return (errors > 0) ? 1 : 0;
}
