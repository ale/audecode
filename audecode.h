/*
 * audecode - a simple audio decoding library.
 * Copyright (C) 2015 <ale@incal.net>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 *  02110-1301 USA.
 */

/** \file
 * This library exposes audio decoding functionality as a (buffered)
 * binary-stream interface. It uses the `avresample' library to ensure
 * the output is in a specific format.
 *
 * Data is fed to the library using a read callback function, and the
 * output is read using a simple read() interface.
 *
 * CAVEATS / LIMITATIONS
 *
 * Only one audio stream can be extracted from the input: this will be
 * the `best' one according to libav.
 */

#ifndef __audecode_h
#define __audecode_h 1

#include <sys/types.h>
#include <stdint.h>

struct audecode;

/**
 * Audio decoding context.
 */
typedef struct audecode *audecode_t;

struct audecode_metadata_iterator;

/**
 * Metadata iterator.
 */
typedef struct audecode_metadata_iterator *audecode_metadata_iterator_t;

/**
 * Callback to read input data. This function will be called with the
 * \a opaque pointer passed to \a audecode_new() and a memory buffer
 * (specified using pointer and size) as parameters. It should store at
 * most the requested number of bytes in the provided buffer, and it
 * should return the number of bytes read.
 */
typedef int (*audecode_read_callback_t) (void*, uint8_t*, int);

#define AUERR_NO_AUDIO_STREAM            -10
#define AUERR_OUTPUT_NOT_INITIALIZED     -11
#define AUERR_OUTPUT_ALREADY_INITIALIZED -12
#define AUERR_CODEC_NOT_SUPPORTED        -13
#define AUERR_SAMPLE_WIDTH_NOT_SUPPORTED -14

/**
 * Initialize the audecode library. Performs one-time initialization
 * tasks.
 *
 * \warning Must only be called once at program startup time.
 */
void audecode_init(void);

/**
 * Returns a string describing the given error. Buffer space for the
 * resulting string must be passed by the caller in \a errbuf and \a
 * bufsz.
 *
 * On success, the function returns 0.  If \a bufsz is too
 * small, an error will be returned.
 *
 * \param err Error code returned by one of the audecode functions.
 * \param errbuf Buffer space for the error message.
 * \param bufsz Size of \a errbuf .
 */
int audecode_strerror(int err, char *errbuf, int bufsz);

/**
 * Initialize a new decode context.
 *
 * Audecode will use the \a read_cb callback to obtain input data for
 * decoding, passing a buffer to it.
 *
 * Once processing is done, the caller should release the resources
 * associated with the decoding context by calling \a
 * audecode_close().
 *
 * \param[out] outp Pointer to the newly created \a audecode_t object.
 * \param[in] bufsize Size of the input buffer.
 * \param[in] read_cb Callback function to fetch new data.
 * \param[in] opaque Opaque pointer passed to \a read_cb .
 */
int audecode_new(audecode_t *outp, size_t bufsize, audecode_read_callback_t read_cb, void *opaque);

/**
 * Return audio parameters of the input stream.
 *
 * \param[in] p Audio decoding context.
 * \param[out] out_sample_rate Sample rate (Hz).
 * \param[out] out_channels Number of channels.
 * \param[out] out_bytes_per_sample Number of bytes per sample.
 */
void audecode_get_input_params(audecode_t p, int *out_sample_rate, int *out_channels, int *out_bytes_per_sample);

/**
 * Return codec parameters of the input stream.
 *
 * \param[in] p Audio decoding context.
 * \param[out] out_codec
 * \parblock
 * Codec name, a short string. The memory holding the string can be
 * used as long as \a audecode_close() is not called.
 * \endparblock
 * \param[out] out_bitrate Bitrate (bps), if available.
 */
void audecode_get_input_codec_params(audecode_t p, const char **out_codec, int *out_bitrate);

/**
 * Set audio parameters for the output stream.
 *
 * If desired, audecode can transcode the input stream to different
 * audio parameters (might be necessary to downsample inputs to what
 * is supported by the audio device, for example).
 *
 * \warning This function must be called before \a audecode_read() .
 * \param p Audio decoding context.
 * \param sample_rate Output sample rate (Hz).
 * \param channels Output audio channels.
 * \param bytes_per_sample Output bytes per sample.
 */
int audecode_set_output_params(audecode_t p, int sample_rate, int channels, int bytes_per_sample);

/**
 * Free all resources associated with the audio decoding context.
 *
 * \param p Audio decoding context.
 */
void audecode_close(audecode_t p);

/**
 * Decode some data.
 *
 * Return the number of bytes read, or an error (return value < 0).
 *
 * \param p Audio decoding context.
 * \param outbuf Buffer to be filled with decoded data.
 * \param outsz Size of \a outbuf .
 */
int audecode_read(audecode_t p, uint8_t *outbuf, int outsz);

void audecode_dump_metadata(audecode_t p);

/**
 * Return an iterator over the metadata key/value pairs contained
 * within the input file.
 *
 * \param p Audio decoding context.
 */
audecode_metadata_iterator_t audecode_get_metadata(audecode_t p);

/**
 * Advance the metadata iterator. If the iterator has not reached the
 * end yet, the function returns 0 and sets \a key and \a value to the
 * next metadata key/value pair, otherwise it will return -1.
 *
 * \param[in] mdi Iterator.
 * \param[out] key Current metadata key.
 * \param[out] value Current metadata value.
 */
int audecode_metadata_next(audecode_metadata_iterator_t mdi, char **key, char **value);

#endif
