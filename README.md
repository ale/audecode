

audecode
========

*audecode* exposes audio decoding functionality as a (buffered)
binary-stream interface. It can convert the output to the desired
sample format (sample rate / sample width / channel downmixing).

Data is fed to the library using a read callback function, and the
output is read using a simple *read()*-like interface.

## Installation

This library depends on the `libav*` set of libraries `libavcodec`,
`libavformat`, `libavutil` (either from ffmpeg or libav). On a
Debian-based system, the following command should install all the
required dependencies:

    $ sudo apt-get install libavcodec-dev libavformat-dev \
        libavutil-dev libavresample-dev

Note that if you are using the ffmpeg packages you might have to
replace `libavresample-dev` with `libswresample-dev`.

The code is built using *autoconf*, so the standard installation
procedure applies:

    $ sh ./autogen.sh
    $ ./configure
    $ make && sudo make install

## Caveats / Limitations

Only one audio stream can be extracted from the input: this will be
the `best' one according to libav.

