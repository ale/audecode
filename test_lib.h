/*
 * Common code for tests.
 */


#define TEST_INPUT "testdata/test.flac"
#define TEST_NUM_SAMPLES 138526
#define TEST_INPUT_SIZE 656450

#include <string.h>

static int read_bytes;

struct fdctx {
  int fd;
};

static int my_read_cb(void *opaque, uint8_t *buf, int buf_size) {
  struct fdctx *ctx = (struct fdctx *)opaque;
  int n = read(ctx->fd, buf, buf_size);
  if (n > 0) {
    read_bytes += n;
  }
}

void die(const char *tag, int error) {
  char errbuf[256];
  audecode_strerror(error, errbuf, sizeof(errbuf));
  fprintf(stderr, "TEST ERROR: %s: %s\n", tag, errbuf);
  exit(1);
}

void die_msg(const char *s) {
  fprintf(stderr, "TEST ERROR: %s\n", s);
  exit(1);
}

char *path_of(const char *base) {
  char *srcdir = getenv("srcdir");
  char *out = (char *)malloc(2 + strlen(srcdir) + strlen(base));
  sprintf(out, "%s/%s", srcdir, base);
  return out;
}
