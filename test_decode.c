/*
 * Example program for libaucodec: converts the given file to
 * 44100/16bit/stereo and outputs the result to stdout.
 */

#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include "audecode.h"
#include "test_lib.h"

void decode_input(int sample_rate, int channels, int bytes_per_sample, uint8_t **obuf, int *osz) {
  audecode_t p;
  struct fdctx ctx;
  int r, sz = 65536;
  uint8_t buf[sz];

  *obuf = (uint8_t *)malloc(1);
  *osz = 0;

  read_bytes = 0;
  ctx.fd = open(path_of(TEST_INPUT), O_RDONLY);
  r = audecode_new(&p, 4096, &my_read_cb, (void *)&ctx);
  if (r < 0) {
    die("audecode_new", r);
  }

  audecode_set_output_params(p, sample_rate, channels, bytes_per_sample);

  while (1) {
    int r = audecode_read(p, buf, sz);
    printf("test_read: r=%d\n", r);
    if (r < 0) {
      break;
    } else if (r == 0) {
      fprintf(stderr, "empty read!\n");
      continue;
    }
    *obuf = (uint8_t *)realloc(*obuf, *osz + r);
    memcpy(*obuf + *osz, buf, r);
    *osz += r;
  }

  audecode_close(p);
  close(ctx.fd);
}

static int sliding_compare(uint8_t *outer, int outer_sz, uint8_t *inner, int inner_sz) {
  int off = outer_sz - inner_sz, i;

  if (off < 0) {
    return -1;
  }

  for (i = 0; i < off; i++) {
    if (0 == memcmp(outer + i, inner, inner_sz)) {
      return 0;
    }
  }
  return -1;
}

static int compare(uint8_t *a, int asz, uint8_t *b, int bsz, int *diffpos) {
  int i;
  if (asz != bsz) {
    return -1;
  }
  for (i = 0; i < asz; i++) {
    if (a[i] != b[i]) {
      if (diffpos != NULL) {
        *diffpos = i;
      }
      return -1;
    }
  }
  return 0;
}

static void print_buffer(char *tag, uint8_t *buf, int sz) {
  int i;
  printf("%s: ", tag);
  if (sz > 40)
    sz = 40;
  for (i = 0; i < sz; i++) {
    printf("%02x ", buf[i]);
  }
  printf("\n");
}

static int compare_with_file(uint8_t *buf, int sz, const char *filename) {
  char filepath[4096];
  FILE *fp;
  long filesz;
  int n, diffpos, ret = 0;
  uint8_t *filedata;

  sprintf(filepath, "%s/%s", getenv("srcdir"), filename);

  fp = fopen(filepath, "r");
  if (fp == NULL) {
    return -1;
  }
  fseek(fp, 0, SEEK_END);
  filesz = ftell(fp);
  fseek(fp, 0, SEEK_SET);
  filedata = (uint8_t *)malloc(filesz);
  if ((n = fread(filedata, 1, filesz, fp)) != filesz) {
    fprintf(stdout, "ERROR: %s: short read\n", filepath);
    ret = -1;
    goto error;
  }

  if (filesz != sz) {
    fprintf(stdout, "WARNING: expected size %ld, got %d\n", filesz, sz);
  }
  
  if (0 != compare(filedata, filesz, buf, sz, &diffpos)) {
    fprintf(stdout, "ERROR: buffer contents differ at byte %d\n", diffpos);
    print_buffer("reference", filedata, filesz);
    print_buffer("output", buf, sz);
    ret = -1;
    goto error;
  }

 error:
  fclose(fp);
  free(filedata);
  return ret;
}

struct test_run {
  int sample_rate, channels, bytes_per_sample;
  int expected_size;
  const char *reference_file;
};

int test_decode() {
  int errors = 0;
  uint8_t *buf;
  int sz, target_sz;

  struct test_run *cur_test;
  struct test_run test_data[] = {
    {44100, 2, 3, TEST_NUM_SAMPLES * 8, NULL},
    {44100, 2, 2, TEST_NUM_SAMPLES * 4, "testdata/test.raw"},
    {22050, 2, 2, TEST_NUM_SAMPLES * 2 - 8192, "testdata/test-22050.raw"},
    {11025, 2, 2, TEST_NUM_SAMPLES - 4094, "testdata/test-11025.raw"},
    {44100, 1, 2, TEST_NUM_SAMPLES * 2, "testdata/test-mono.raw"},
    {0, 0, 0, 0, NULL}
  };

  for (cur_test = test_data; cur_test->sample_rate > 0; cur_test++) {
    char tag[512];
    sprintf(tag, "%d/%d/%d", cur_test->sample_rate, cur_test->channels, cur_test->bytes_per_sample);
  
    decode_input(cur_test->sample_rate, cur_test->channels, cur_test->bytes_per_sample, &buf, &sz);
    if (read_bytes != TEST_INPUT_SIZE) {
      fprintf(stdout, "ERROR: %s: did not read entire input (%d out of %d)\n", tag, read_bytes, TEST_INPUT_SIZE);
      errors++;
    }

    if (sz != cur_test->expected_size) {
      fprintf(stdout, "ERROR: %s: bad output size: %d (expected %d)\n", tag, sz, cur_test->expected_size);
      errors++;
    }

    if (cur_test->reference_file) {
      if (0 != compare_with_file(buf, sz, cur_test->reference_file)) {
        fprintf(stdout, "ERROR: %s: the contents of %s differ from the output\n", tag, cur_test->reference_file);
        errors++;
      }
    }

    free(buf);
  }

  return errors;
}

int test_codec_params() {
  audecode_t p;
  const char *codec = NULL;
  int bit_rate = 0;
  struct fdctx ctx;
  int r, ret = 0;

  ctx.fd = open(path_of(TEST_INPUT), O_RDONLY);
  r = audecode_new(&p, 4096, &my_read_cb, (void *)&ctx);
  if (r < 0) {
    die("audecode_new", r);
  }

  audecode_get_input_codec_params(p, &codec, &bit_rate);
  if (codec == NULL) {
    fprintf(stdout, "ERROR: 'codec' is empty\n");
    ret = 1;
  }
  if (strcmp(codec, "flac") != 0) {
    fprintf(stdout, "ERROR: 'codec' is '%s', not 'flac'\n", codec);
    ret = 1;
  }
  if (bit_rate != 0) {
    fprintf(stdout, "ERROR: 'bit_rate' is not empty (%d)\n", bit_rate);
    ret = 1;
  }

  audecode_close(p);

  return ret;
}

int main(int argc, char **argv) {
  int errors = 0;

  audecode_init();

  fprintf(stdout, "test_decode\n");
  errors += test_decode();
  fprintf(stdout, "test_codec_params\n");
  errors += test_codec_params();

  return (errors > 0) ? 1 : 0;
}
  
