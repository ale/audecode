/*
 * audecode - a simple audio decoding library.
 * Copyright (C) 2015 <ale@incal.net>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 *  02110-1301 USA.
 */

#include "config.h"

#include <libavutil/samplefmt.h>
#include <libavutil/opt.h>
#include <libavutil/mathematics.h>
#include <libavformat/avformat.h>
#if HAVE_AVRESAMPLE
# include <libavresample/avresample.h>
#elif HAVE_SWRESAMPLE
# include <libswresample/swresample.h>
#else
# error Need either libavresample or libswresample
#endif

#include "audecode.h"

/* Most mp3s won't be detected with a smaller probe size. */
#define AUDECODE_PROBE_SIZE (2 << 20) /* 2MB */

struct audecode {
  unsigned char *io_buf;
  AVIOContext *io_ctx;
  AVFormatContext *fmt;
  AVStream *stream;
  int stream_idx;
  AVFrame *frame;
  AVPacket pkt, pkt_temp;
  int64_t pos;

#if HAVE_AVRESAMPLE
  AVAudioResampleContext *avr;
#elif HAVE_SWRESAMPLE
  SwrContext *avr;
#endif
  int output_sample_rate;
  int output_channels;
  int output_bytes_per_sample;
  int output_sample_fmt;
  int source_eof;

  int input_samples;

  uint8_t *buf;
  int buf_size;
  int buf_pos;
};

int audecode_strerror(int err, char *errbuf, int bufsz) {
  int r = 0;
  switch (err) {
  case AUERR_NO_AUDIO_STREAM:
    strncpy(errbuf, "could not find an audio stream", bufsz);
    break;
  case AUERR_OUTPUT_NOT_INITIALIZED:
    strncpy(errbuf, "output parameters not initialized", bufsz);
    break;
  case AUERR_OUTPUT_ALREADY_INITIALIZED:
    strncpy(errbuf, "output parameters already initialized", bufsz);
    break;
  case AUERR_CODEC_NOT_SUPPORTED:
    strncpy(errbuf, "codec not supported", bufsz);
    break;
  case AUERR_SAMPLE_WIDTH_NOT_SUPPORTED:
    strncpy(errbuf, "sample width not supported", bufsz);
    break;
  default:
    r = av_strerror(err, errbuf, bufsz);
  }
  errbuf[bufsz-1] = '\0';
  return r;
}

int audecode_new(audecode_t *outp, size_t bufsize, audecode_read_callback_t read_cb, void *opaque) {
  audecode_t p = (audecode_t)calloc(1, sizeof(struct audecode));
  AVDictionary *dict = NULL;
  AVCodec *decoder;
  int r, idx;

  p->io_buf = av_malloc(bufsize);
  p->io_ctx = avio_alloc_context(p->io_buf, bufsize, 0, opaque, read_cb, NULL, NULL);
  p->io_ctx->seekable = 0;

  p->fmt = avformat_alloc_context();
  p->fmt->pb = p->io_ctx;
  p->fmt->probesize = AUDECODE_PROBE_SIZE;
  if ((r = avformat_open_input(&p->fmt, "stream", NULL, NULL)) < 0) {
    goto err;
  }

  av_dump_format(p->fmt, 0, "input", 0);

  /* We have opened the file. Find the 'best' audio stream. */
  if ((r = avformat_find_stream_info(p->fmt, NULL)) < 0) {
    goto err;
  }
  idx = av_find_best_stream(p->fmt, AVMEDIA_TYPE_AUDIO, -1, -1, NULL, 0);
  if (idx < 0) {
    r = AUERR_NO_AUDIO_STREAM;
    goto err;
  }
  p->stream_idx = idx;

  p->stream = p->fmt->streams[idx];
  decoder = avcodec_find_decoder(p->stream->codec->codec_id);
  if (decoder == NULL) {
    r = AUERR_CODEC_NOT_SUPPORTED;
    goto err;
  }

  if ((r = avcodec_open2(p->stream->codec, decoder, &dict)) < 0) {
    goto err;
  }
  av_init_packet(&p->pkt);
  p->pkt.data = NULL;
  p->pkt.size = 0;

  p->frame = avcodec_alloc_frame();

  *outp = p;
  return 0;

 err:
  audecode_close(p);
  return r;
}

int audecode_set_output_params(audecode_t p, int sample_rate, int channels, int bytes_per_sample) {
  int r, sample_fmt, input_channel_layout;

  switch (bytes_per_sample) {
  case 1:
    sample_fmt = AV_SAMPLE_FMT_U8;
    break;
  case 2:
    sample_fmt = AV_SAMPLE_FMT_S16;
    break;
  case 3:
  case 4:
    sample_fmt = AV_SAMPLE_FMT_S32;
    break;
  default:
    return AUERR_SAMPLE_WIDTH_NOT_SUPPORTED;
  }

  /*
   * Occasionally libavformat isn't able to detect the input channel
   * layout. Use the default layout for the given number of channels.
   */
  input_channel_layout = p->stream->codec->channel_layout;
  if (input_channel_layout == 0) {
    input_channel_layout = av_get_default_channel_layout(p->stream->codec->channels);
  }

  if (p->avr != NULL) {
    return AUERR_OUTPUT_ALREADY_INITIALIZED;
  }

  p->output_sample_rate = sample_rate;
  p->output_sample_fmt = sample_fmt;
  p->output_channels = channels;
  p->output_bytes_per_sample = bytes_per_sample;

#if HAVE_AVRESAMPLE
  p->avr = avresample_alloc_context();

  av_opt_set_int(p->avr, "in_channel_layout", input_channel_layout, 0);
  av_opt_set_int(p->avr, "in_sample_rate", p->stream->codec->sample_rate, 0);
  av_opt_set_int(p->avr, "in_sample_fmt", p->stream->codec->sample_fmt, 0);
  av_opt_set_int(p->avr, "out_channel_layout", av_get_default_channel_layout(channels), 0);
  av_opt_set_int(p->avr, "out_sample_rate", sample_rate, 0);
  av_opt_set_int(p->avr, "out_sample_fmt", sample_fmt, 0);

  av_opt_set_int(p->avr, "dither_method", (sample_rate == 44100 || sample_rate == 48000) ? AV_RESAMPLE_DITHER_TRIANGULAR_NS : AV_RESAMPLE_DITHER_TRIANGULAR_HP, 0);
  av_opt_set_int(p->avr, "filter_type", AV_RESAMPLE_FILTER_TYPE_KAISER, 0);

  if ((r = avresample_open(p->avr)) < 0) {
    return r;
  }

#elif HAVE_SWRESAMPLE
  p->avr = swr_alloc_set_opts(NULL,
                              av_get_default_channel_layout(channels),
                              sample_fmt,
                              sample_rate,
                              input_channel_layout,
                              p->stream->codec->sample_fmt,
                              p->stream->codec->sample_rate,
                              0, NULL);
#endif

  return 0;
}

void audecode_get_input_params(audecode_t p, int *out_sample_rate, int *out_channels, int *out_bytes_per_sample) {
  if (out_sample_rate != NULL) {
    *out_sample_rate = p->stream->codec->sample_rate;
  }
  if (out_channels != NULL) {
    *out_channels = p->stream->codec->channels;
  }
  if (out_bytes_per_sample != NULL) {
    *out_bytes_per_sample = av_get_bytes_per_sample(p->stream->codec->sample_fmt);
  }
}

void audecode_get_input_codec_params(audecode_t p, const char **out_codec, int *out_bitrate) {
  if (out_bitrate != NULL) {
    *out_bitrate = p->stream->codec->bit_rate;
  }
  if (out_codec != NULL) {
    *out_codec = p->stream->codec->codec->name;
  }
}

void audecode_close(audecode_t p) {
  if (p->pkt.data) {
    av_free_packet(&p->pkt);
  }
  if (p->avr) {
#if HAVE_AVRESAMPLE
    avresample_free(&p->avr);
#elif HAVE_SWRESAMPLE
    swr_free(&p->avr);
#endif
  }
  if (p->stream) {
    avcodec_close(p->stream->codec);
  }
  if (p->frame) {
    avcodec_free_frame(&p->frame);
  }
  if (p->fmt) {
    avformat_close_input(&p->fmt);
  }
  if (p->io_ctx) {
    av_free(p->io_ctx);
  }
  if (p->buf) {
    av_free(p->buf);
  }
  free(p);
}

void audecode_init() {
  av_register_all();
}

static int audecode_read_packet(audecode_t p) { 
  int r, out_size = 0, out_pos = 0, in_total = 0;

  if (p->source_eof) {
    return -1;
  }

  do {
    AVPacket packet, orig;

    while (1) {
      if ((r = av_read_frame(p->fmt, &packet)) < 0) {
        /* End of file. */
        p->source_eof = 1;
        break;
      }
      if (packet.stream_index == p->stream_idx) {
        break;
      }
      av_free_packet(&packet);
    }

    orig = packet;

    while (packet.size > 0 || p->source_eof) {
      int chunk_size, out_samples, out_linesize, in_used, got_frame = 0;
      uint8_t *out;

      if (p->source_eof) {
        out_samples = avresample_available(p->avr);
        chunk_size = av_samples_get_buffer_size(&out_linesize,
                                                p->output_channels,
                                                out_samples,
                                                p->output_sample_fmt, 0);
        p->buf = av_realloc(p->buf, out_pos + chunk_size);
        out = p->buf + out_pos;
        out_samples = avresample_read(p->avr, &out, out_samples);
      } else {
        avcodec_get_frame_defaults(p->frame);

        in_used = avcodec_decode_audio4(p->stream->codec,
                                        p->frame,
                                        &got_frame,
                                        &packet);
        if (in_used < 0) {
          /* Decoding error - skip frame. */
          break;
        }
        p->input_samples += p->frame->nb_samples;

        packet.data += in_used;
        packet.size -= in_used;
        in_total += in_used;

        if (!got_frame) {
          continue;
        }

        chunk_size = av_samples_get_buffer_size(&out_linesize,
                                                p->output_channels,
                                                p->frame->nb_samples,
                                                p->output_sample_fmt, 0);
        p->buf = av_realloc(p->buf, out_pos + chunk_size);
        out = p->buf + out_pos;

        out_samples = (avresample_available(p->avr) +
                       av_rescale_rnd(avresample_get_delay(p->avr) + 
                                      p->frame->nb_samples,
                                      p->output_sample_rate,
                                      p->stream->codec->sample_rate,
                                      AV_ROUND_UP));
        out_samples = avresample_convert(p->avr,
                                         &out,
                                         out_linesize,
                                         out_samples,
                                         p->frame->data,
                                         p->frame->linesize[0],
                                         p->frame->nb_samples);
      }

      if (out_samples < 0) {
        /* EOF. */
        return -1;
      }
      out_size += out_samples * av_get_bytes_per_sample(p->output_sample_fmt) * p->output_channels;
      out_pos += out_size;

      if (out_samples == 0) {
        break;
      }
    }

    if (!p->source_eof) {
      p->pos = packet.pts;
      av_free_packet(&orig);
    }
  } while (out_size == 0 && !p->source_eof);

  p->buf_size = out_size;
  p->buf_pos = 0;

  return out_size;
}

int audecode_read(audecode_t p, uint8_t *outbuf, int outsz) {
  int n, r, tot = 0;

  if (p->avr == NULL) {
    return AUERR_OUTPUT_NOT_INITIALIZED;
  }

  while (outsz > 0) {
    if (p->buf_pos < p->buf_size) {
      n = p->buf_size - p->buf_pos;
      if (n > outsz) {
        n = outsz;
      }
      memcpy(outbuf, p->buf + p->buf_pos, n);
      outsz -= n;
      outbuf += n;
      p->buf_pos += n;
      tot += n;
    }
    r = audecode_read_packet(p);
    if (r < 0) {
      break;
    }
  }
  if (tot > 0) {
    return tot;
  }
  /* EOF. */
  return -1;
}

static void print_metadata(AVDictionary *meta) {
  AVDictionaryEntry *entry = NULL;
  /* Iterate over all keys in the metadata dictionary. */
  while ((entry = av_dict_get(meta, "", entry, AV_DICT_IGNORE_SUFFIX)) != NULL) {
    fprintf(stderr, "key=%s value=%s\n", entry->key, entry->value);
  }
}

struct audecode_metadata_iterator {
  audecode_t p;
  AVDictionary *meta;
  AVDictionaryEntry *entry;
};

typedef struct audecode_metadata_iterator *audecode_metadata_iterator_t;

void audecode_dump_metadata(audecode_t p) {
  fprintf(stderr, "\nFormat metadata:\n");
  print_metadata(p->fmt->metadata);
  fprintf(stderr, "\nStream metadata:\n");
  print_metadata(p->stream->metadata);
}

audecode_metadata_iterator_t audecode_get_metadata(audecode_t p) {
  audecode_metadata_iterator_t mdi = (audecode_metadata_iterator_t)malloc(sizeof(struct audecode_metadata_iterator));
  mdi->p = p;
  if (p->fmt->metadata && av_dict_count(p->fmt->metadata) > 0) {
    mdi->meta = p->fmt->metadata;
  } else {
    mdi->meta = p->stream->metadata;
  }
  mdi->entry = NULL;
  return mdi;
}

int audecode_metadata_next(audecode_metadata_iterator_t mdi, char **key, char **value) {
  mdi->entry = av_dict_get(mdi->meta, "", mdi->entry, AV_DICT_IGNORE_SUFFIX);
  if (mdi->entry == NULL) {
    return -1;
  }
  *key = mdi->entry->key;
  *value = mdi->entry->value;
  return 0;
}
