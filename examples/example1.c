/*
 * Example program for libaucodec: converts the given file to
 * 44100/16bit/stereo and outputs the result to stdout.
 */

#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include "audecode.h"

struct fdctx {
  int fd;
};

static int my_read_cb(void *opaque, uint8_t *buf, int buf_size) {
  struct fdctx *ctx = (struct fdctx *)opaque;
  return read(ctx->fd, buf, buf_size);
}

void die(int error) {
  char errbuf[256];
  audecode_strerror(error, errbuf, sizeof(errbuf));
  fprintf(stderr, "error: %s\n", errbuf);
  exit(1);
}

int main(int argc, char **argv) {
  char *filename;
  int sample_rate = 44100, channels = 2, bytes_per_sample = 2;
  int c, r;
  audecode_t p;
  struct fdctx ctx;
  int sz = 65536;
  uint8_t *buf = (uint8_t *)malloc(sz);

  while ((c = getopt(argc, argv, "r:c:b:")) > 0) {
    switch (c) {
    case 'r':
      sample_rate = atoi(optarg);
      break;
    case 'c':
      channels = atoi(optarg);
      break;
    case 'b':
      bytes_per_sample = atoi(optarg);
      break;
    default:
      printf("Unknown option '%c'\n", c);
      exit(1);
    }
  }

  if (optind >= argc) {
    printf("Need a filename as argument.\n");
    exit(1);
  }
  filename = argv[optind];

  audecode_init();

  ctx.fd = open(filename, O_RDONLY);
  r = audecode_new(&p, 4096, &my_read_cb, (void *)&ctx);
  if (r < 0) {
    die(r);
  }

  audecode_dump_metadata(p);

  audecode_set_output_params(p, sample_rate, channels, bytes_per_sample);

  while (1) {
    int r = audecode_read(p, buf, sz);
    if (r <= 0) {
      break;
    }
    fwrite(buf, 1, r, stdout);
  }

  close(ctx.fd);
  free(buf);
  audecode_close(p);

  return 0;
}
